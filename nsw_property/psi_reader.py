"""
Methods for reading the PSI files output by the valuer general website.
"""
import typing

import pandas
from fs import zipfs, walk, osfs, base
from tqdm import tqdm


def crawl_psi_zip_dir(
    directory_with_zips: str, cache_location: base.FS = None
) -> pandas.DataFrame:
    if cache_location is None:
        return _crawl_psi_zip_dir(directory_with_zips)
    else:
        if not cache_location.exists("crawl.pkl"):
            res = _crawl_psi_zip_dir(directory_with_zips)
            with cache_location.open("crawl.pkl", "wb") as f:
                res.to_pickle(f, compression=None)
        with cache_location.open("crawl.pkl", "rb") as f:
            # noinspection PyTypeChecker
            return pandas.read_pickle(f, compression=None)


def _crawl_psi_zip_dir(directory_with_zips):
    zips_dir = osfs.OSFS(directory_with_zips, create=False)
    results = []
    for step in zips_dir.walk():
        for f in tqdm(step.files):
            if f.name.endswith(".zip"):
                fname = zips_dir.getsyspath(f.name)
                results.append(craw_psi_zip_file(fname))
            else:
                fname = zips_dir.geturl(f.name)
                print(f"Unknown / unhandled file: {fname}")

    res = pandas.concat(results).reset_index(drop=True)
    res.price = pandas.to_numeric(res.price)
    res.contract_date = pandas.to_datetime(
        res.contract_date, format="%Y%m%d", errors="coerce"
    )
    res.settlement_date = pandas.to_datetime(
        res.settlement_date, format="%Y%m%d", errors="coerce"
    )
    res.area = pandas.to_numeric(res.area)

    return res


def craw_psi_zip_file(filename: str) -> pandas.DataFrame:
    fs = zipfs.ReadZipFS(filename)
    results = []
    for step in fs.walk():
        assert isinstance(step, walk.Step)
        for f in step.files:
            if f.name.endswith(".DAT"):
                with fs.open(f.name, "r") as r:
                    # noinspection PyTypeChecker
                    results.append(read_psi_file(r))

    return pandas.concat(results)


StrFile = typing.Union[str, typing.TextIO]


def read_psi_file(file_handle: StrFile) -> pandas.DataFrame:
    finally_close = False
    if not hasattr(file_handle, "read"):
        file_handle = open(file_handle)
        finally_close = True

    try:
        res = _read_from_handle(file_handle)
    except Exception:
        if finally_close:
            file_handle.close()
        raise

    return pandas.concat(res, 1).T


def _read_from_handle(file_handle: typing.TextIO):
    result_entries = []
    for line in file_handle.readlines():
        entries = line.split(";")
        if entries[0] == "A":
            pass
        elif entries[0] == "B":
            try:
                (
                    rt,
                    dc,
                    pid,
                    sc,
                    ts,
                    pn,
                    un,
                    hn,
                    sn,
                    pl,
                    ppc,
                    area,
                    at,
                    cd,
                    sd,
                    pp,
                    z,
                    np,
                    purp,
                    *_,
                ) = entries

                new_entry = {
                    "name": pn,
                    "unitnum": un,
                    "housenum": hn,
                    "street": sn,
                    "suburb": pl,
                    "postcode": ppc,
                    "area": area,
                    "areaunit": at,
                    "contract_date": cd,
                    "settlement_date": sd,
                    "price": pp,
                    "zoning": z,
                    "nature": np,
                    "nature_other": purp,
                }

                result_entries.append(pandas.Series(new_entry))
            except Exception:
                print(f"Error parsing line: {line}")
                raise
        elif entries[0] == "C":
            pass
        elif entries[0] == "D":
            pass
        elif entries[0] == "Z":
            pass
        else:
            raise ValueError(f"Bad record: {line}")
    return result_entries


if __name__ == "__main__":
    from fs.appfs import UserCacheFS

    cache_fs = UserCacheFS("nsw-property")
    resfs = crawl_psi_zip_dir("/Users/riri/Desktop/2018/", cache_fs)
    print(resfs)
