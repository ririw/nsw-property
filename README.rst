nsw_property
============

Tools and analysis of NSW property data

Usage
-----

Installation
------------

Requirements
^^^^^^^^^^^^

Compatibility
-------------

Licence
-------

Authors
-------

`nsw_property` was written by `Richard Weiss <richardweiss@richardweiss.org>`_.
